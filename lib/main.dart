import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/gestures.dart';
import 'wp-api.dart';
import 'package:path_provider/path_provider.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.storage.request();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
  );
  runApp(MyApp());
}

/////// FUNCTIONS ///////
pageCard(color, fa, title, content, link, context) {
  return
    Card(
      margin: EdgeInsets.all(5.0),
      child: FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, link);
        },
        padding: EdgeInsets.all(0),
        child: Row(
          children: [
            Container(
              width: 80,
              height: 80,
              color: Color(color),
              child: Center(
                child: FaIcon(
                  fa,
                  color: Colors.white,
                  size: 28.0,
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB( 20, 0, 0, 5),
                    child:
                    Text(
                      title,
                      style: TextStyle(
                        color: Color(0xFF053876),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  if(content != '')
                    Padding(
                      padding: EdgeInsets.fromLTRB( 20, 0, 0, 0),
                      child:
                      Text(
                        content,
                        style: TextStyle(
                          color: Color(0xFF053876),
                          fontSize: 14,
                        ),
                      ),
                    ),
                ],
              ),
            ),
            Container(
              width: 50,
              child: Center(
                child: FaIcon(
                  FontAwesomeIcons.angleRight,
                  color: Color(0xFF053876),
                  size: 36.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
}

/////// FUNCTIONS ///////
extPageCard(color, fa, title, content, page, link, context) {
  return
    Card(
      margin: EdgeInsets.all(5.0),
      child: FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, page, arguments: link);
        },
        padding: EdgeInsets.all(0),
        child: Row(
          children: [
            Container(
              width: 80,
              height: 80,
              color: Color(color),
              child: Center(
                child: FaIcon(
                  fa,
                  color: Colors.white,
                  size: 28.0,
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB( 20, 0, 0, 5),
                    child:
                    Text(
                      title,
                      style: TextStyle(
                        color: Color(0xFF053876),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  if(content != '')
                    Padding(
                      padding: EdgeInsets.fromLTRB( 20, 0, 0, 0),
                      child:
                      Text(
                        content,
                        style: TextStyle(
                          color: Color(0xFF053876),
                          fontSize: 14,
                        ),
                      ),
                    )
                ],
              ),
            ),
            Container(
              width: 50,
              child: Center(
                child: FaIcon(
                  FontAwesomeIcons.angleRight,
                  color: Color(0xFF053876),
                  size: 36.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
}


//////// My App ////////
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/home': (context) => MyHome(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/dont': (context) => DontBe(),
        '/report': (context) => Report(),
        '/about': (context) => About(),
        '/external': (context) => External(),
        '/news': (context) => News(),
        '/posts': (context) => Posts(),
      },
      home: MyHome(),
    );
  }
}

/////// PAGES ////////

// Homepage //
class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            toolbarHeight: 80,
            backgroundColor: Color(0xFF053876),
            brightness: Brightness.dark,
            centerTitle: true,
            title: Image.asset(
              "images/new-logo.png",
              height: 43,
              fit: BoxFit.contain,
            ),
          ),
          body: SingleChildScrollView(
            child:
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    extPageCard(0xFF8ccc00, FontAwesomeIcons.lightHeart, 'Love Portsmouth', 'Don\'t be a din', '/external', 'https://www.portsmouth.gov.uk/2020/07/03/dont-be-a-din-put-it-in-the-bin/',  context),
                    pageCard(0xFF053876, FontAwesomeIcons.comment, 'Report it', 'Report a problem or issue to us', '/report', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.fileAlt, 'Penalty charge notice', 'Pay your PCN online', '/external', 'https://parkingtickets.portsmouth.gov.uk/PortsmouthOCMPresentation20130715/Default.aspx', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.calendarAlt, 'Bin collection', 'Find out when your rubbish and recycling is collected', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://AF-Process-26e27e70-f771-47b1-a34d-af276075cede/AF-Stage-cd7cc291-2e59-42cc-8c3f-1f93e132a2c9/definition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.dumpster, 'Bulky waste', 'Enquire about a bulky waste collection', '/external', 'https://www.portsmouth.gov.uk/services/recycling-and-rubbish/large-household-items/', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.users, 'Councillors', 'Find information about your councillors', '/external', 'https://democracy.portsmouth.gov.uk/mgMemberIndex.aspx?bcr=1', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.graduationCap, 'Schools', 'Find information on schools in Portsmouth', '/external', 'https://www.portsmouth.gov.uk/ext/schools-learning-and-childcare/schools/schools', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.briefcase, 'Jobs', 'Find the latest jobs available with the council', '/external', 'https://careers.portsmouth.gov.uk/', context),
                    pageCard(0xFF053876, FontAwesomeIcons.newspaper, 'News', 'All the latest news from your council', '/news', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.facebook, 'Facebook', 'Check out our page and give us a like', '/external', 'https://www.facebook.com/Portsmouthcitycouncil/', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.twitter, 'Twitter', 'Follow us and send us a tweet', '/external', 'https://twitter.com/portsmouthtoday', context),
                    pageCard(0xFF053876, FontAwesomeIcons.questionCircle, 'About us', 'About Portsmouth City Council', '/about', context),
                  ],
                ),
              ),
            ),
          )
      );
  }
}

// Report //
class Report extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            toolbarHeight: 80,
            backgroundColor: Color(0xFF053876),
            brightness: Brightness.dark,
            centerTitle: true,
            title: Image.asset(
              "images/new-logo.png",
              height: 43,
              fit: BoxFit.contain,
            ),
          ),
          body: SingleChildScrollView(
            child:
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    extPageCard(0xFF053876, FontAwesomeIcons.trashAlt, 'Fly Tipping', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-5e2d630c-2331-47fc-ae25-9d4b79c59811%2FAF-Stage-a5024096-d4e1-4221-9493-9c551d91cd99%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Report%20fly%20tipping', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.lightbulb, 'Street Light', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-12ca2205-bf0c-446e-82c4-9d6132f49dbb%2FAF-Stage-5004ae7e-4124-4985-9d6c-25506e12f780%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Street%20light', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.poop, 'Dog fouling', '', '/external', 'https://portsmouthlocaloffer.org/document-hub/', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.tree, 'Tree problem', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-3273442b-ab9b-437c-8461-1f66a19ca2bb%2FAF-Stage-90e1c6cd-621b-41de-adc6-a19c946aaf38%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Tree%20problem', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.hotel, 'HMO issue', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-9bb346b9-5f5c-47d5-8a93-620e57be5e05%2FAF-Stage-652833ab-3b31-47b3-b7c2-7422281bdc30%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=HMO%20issue', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.parking, 'Parking issue', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-eadc4469-a390-44fd-97a5-a64585ac323c%2FAF-Stage-8e0c3cd9-948b-4736-8bb2-3ab1ad5a9e1e%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Parking%20issue', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.houseDamage, 'Dangerous building', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-41a72db5-0d72-4c78-aa66-d0b8121c58e4%2FAF-Stage-dc4ab505-15cc-4ee1-900b-9214797348f1%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Dangerous%20building', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.road, 'Road issue', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-0ee19f0f-ecae-4183-bce6-6e2d969b7a9d%2FAF-Stage-7ada4ede-9faa-4fa3-a82f-9eb82611ec9d%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Pothole', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.car, 'Abandoned vehicle', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-18052511-1797-4611-b82d-51b31ae99084%2FAF-Stage-5b155095-3b7b-48b5-9010-115259eb0b35%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Abandoned%20vehicle', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.waterRise, 'Drainage problems', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish%3A%2F%2FAF-Process-9d2a260e-5bc4-4664-9c54-faae8503f9c7%2FAF-Stage-d5363db4-ba28-4c3e-9cf2-9d2349b3f2f4%2Fdefinition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes&utm_source=MyPortsmouthApp&utm_medium=App&utm_campaign=Drain%20problem', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.boxOpen, 'Litter', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://AF-Process-12d47ef1-a4b0-423b-a327-bdf3311bf7ed/AF-Stage-34111651-8666-4124-9daa-7449ca9acf9d/definition.json&redirectlink=%2Fen&cancelRedirectLink=%2Fen&consentMessage=yes', context),
                    extPageCard(0xFF053876, FontAwesomeIcons.sprayCan, 'Graffiti', '', '/external', 'https://my.portsmouth.gov.uk/en/AchieveForms/?form_uri=sandbox-publish://AF-Process-7ca8cdd6-9930-4d88-b373-8bc828931191/AF-Stage-9bc95f0a-9db7-47a4-8554-d1ceae571707/definition.json&amp;redirectlink=%2Fen&amp;cancelRedirectLink=%2Fen&amp;consentMessage=yes&amp;utm_source=MyPortsmouthApp&amp;utm_medium=App&amp;utm_campaign=Report%20graffiti', context),
                  ],
                ),
              ),
            ),
          )
      );
  }
}

// Don't be a din page //
class DontBe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        backgroundColor: Color(0xFF053876),
        centerTitle: true,
        title: Image.asset(
          "images/new-logo.png",
          height: 43,
          fit: BoxFit.contain,
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
          },
          child: Text('Go Dont!'),
        ),
      ),
    );
  }
}

// About //
class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        backgroundColor: Color(0xFF053876),
        brightness: Brightness.dark,
        centerTitle: true,
        title: Image.asset(
          "images/new-logo.png",
          height: 43,
          fit: BoxFit.contain,
        ),

      ),
      body:
      SingleChildScrollView(
        child:
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  children: [
                    TextSpan(
                        text: 'About us\n\n',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                        )
                    ),
                    TextSpan(
                        text: 'At Portsmouth City Council we are dedicated to keeping the great waterfront city safe, clean and tidy.\n\nAs a significant naval port for hundreds of years, Portsmouth is known for its maritime history, and is home to some of the world\'s most famous ships, such as HMS Warrior, the Mary Rose and Lord Nelson\'s flagship HMS Victory.\n\nWith other key attractions such as the Spinnaker Tower, the eleventh tallest structure in the country, the D-Day museum and Southsea castle, Portsmouth is fast becoming a major destination for people to live, work and visit.'
                    ),
                    TextSpan(
                        text: 'For more information and Portsmouth City Council and our services, please visit our '
                    ),
                    TextSpan(
                      text: 'website\n\n',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, '/external', arguments: 'https://portsmouth.gov.uk');
                        },
                    ),
                    TextSpan(
                        text: 'For me information on how we use your information please visit our '
                    ),
                    TextSpan(
                      text: 'disclaimer statement\n\n',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, '/external', arguments: 'https://www.portsmouth.gov.uk/ext/your-council/policies-and-strategies/disclaimer');
                        },
                    ),
                    TextSpan(
                        text: 'For information on events and attractions, visit our '
                    ),
                    TextSpan(
                      text: 'disclaimer statement',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, '/external', arguments: 'https://www.visitportsmouth.co.uk/');
                        },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// External //
class External extends StatefulWidget {
  @override
  _ExternalState createState() => _ExternalState();

}

class _ExternalState extends State<External> {
  InAppWebViewController _webViewController;

  @override

  Widget build(BuildContext context) {
    final link = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 80,
          backgroundColor: Color(0xFF053876),
          brightness: Brightness.dark,
          centerTitle: true,
          title: Image.asset(
            "images/new-logo.png",
            height: 43,
            fit: BoxFit.contain,
          ),
          actions: [
            IconButton(icon: Icon(Icons.arrow_back),
                onPressed: (){
                  if (_webViewController != null) {
                    _webViewController.goBack();
                  }
                }),
            IconButton(icon: Icon(Icons.arrow_forward),
                onPressed: (){
                  if (_webViewController != null) {
                    _webViewController.goForward();
                  }
                }),
          ],
        ),
        body:
        Container(
          child: InAppWebView(
            initialUrlRequest: URLRequest(url: Uri.parse(link)),
            initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                  useOnDownloadStart: true,
                  javaScriptCanOpenWindowsAutomatically: true,
              ),
            ),
            onWebViewCreated: (InAppWebViewController controller) {
              _webViewController = controller;
              print('webViewCreated');
            },
            onDownloadStart: (InAppWebViewController controller, Uri url) async {
              var appDocDir;
              if(Platform.isAndroid) {
                appDocDir = await getExternalStorageDirectory();
              } else if (Platform.isIOS) {
                appDocDir = await getExternalCacheDirectories();
              }
              String appDocPath = appDocDir.path;
              final taskId = await FlutterDownloader.enqueue(
                url: url.toString(),
                savedDir: appDocPath,
                showNotification: true, // show download progress in status bar (for Android)
                openFileFromNotification: true, // click on notification to open downloaded file (for Android)
              );
            },
            androidOnPermissionRequest: (InAppWebViewController controller, String origin, List<String> resources) async {
              return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
            },
        ),
        )
    );
  }
}

// News //
class News extends StatelessWidget {
  var unescape = HtmlUnescape();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 80,
          backgroundColor: Color(0xFF053876),
          centerTitle: true,
          title: Image.asset(
            "images/new-logo.png",
            height: 43,
            fit: BoxFit.contain,
          ),
        ),
        body: Container(
            child: FutureBuilder(
              future: fetchWpPost(),
              builder: (context, snapshot){
                if(snapshot.hasData) {
                  return Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Map wppost = snapshot.data[index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pushNamed(context, '/external', arguments: wppost['link']);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  unescape.convert(wppost['title']['rendered']),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xFF053876),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                return CircularProgressIndicator();
              },
            )
        )
    );
  }
}

// Post Page //
class Posts extends StatelessWidget {
  var unescape = HtmlUnescape();
  @override
  Widget build(BuildContext context) {
    final postData = ModalRoute.of(context).settings.arguments;
    Map post = postData;
    Map acf = post['acf']['sections'][0];
    Map blocks = acf['blocks'][0];
    print(acf);
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 80,
          backgroundColor: Color(0xFF053876),
          centerTitle: true,
          title: Image.asset(
            "images/new-logo.png",
            height: 43,
            fit: BoxFit.contain,
          ),
        ),
        body:
        SingleChildScrollView(
          child:Container(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Text(
                    unescape.convert(post['title']['rendered']),
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF053876),
                    ),
                  ),
                  (acf['acf_fc_layout'] == 'simple') ?
                  Text(
                    blocks['content'],
                  )
                      :
                  Text(
                    'fail',
                  )
                ],
              ),
            ),
          ),
        )
    );
  }
}


/////////// Widgets ///////////
// class NavigationControls extends StatelessWidget {
//   const NavigationControls(this._webViewControllerFuture)
//       : assert(_webViewControllerFuture != null);
//
//   final Future<WebViewController> _webViewControllerFuture;
//
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<WebViewController>(
//       future: _webViewControllerFuture,
//       builder:
//           (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
//         final bool webViewReady =
//             snapshot.connectionState == ConnectionState.done;
//         final WebViewController controller = snapshot.data;
//         return Row(
//           children: <Widget>[
//             IconButton(
//               icon: const Icon(Icons.arrow_back_ios),
//               onPressed: !webViewReady
//                   ? null
//                   : () async {
//                 if (await controller.canGoBack()) {
//                   await controller.goBack();
//                 } else {
//                   // ignore: deprecated_member_use
//                   Scaffold.of(context).showSnackBar(
//                     const SnackBar(content: Text("No back history item")),
//                   );
//                   return;
//                 }
//               },
//             ),
//             IconButton(
//               icon: const Icon(Icons.arrow_forward_ios),
//               onPressed: !webViewReady
//                   ? null
//                   : () async {
//                 if (await controller.canGoForward()) {
//                   await controller.goForward();
//                 } else {
//                   // ignore: deprecated_member_use
//                   Scaffold.of(context).showSnackBar(
//                     const SnackBar(
//                         content: Text("No forward history item")),
//                   );
//                   return;
//                 }
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }
// }
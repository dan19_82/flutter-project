import 'package:http/http.dart' as http;
import 'dart:convert';

Future<List> fetchWpPost() async {
  var url = 'https://portsmouth.gov.uk/wp-json/wp/v2/posts?per_page=20';
  final response = await http.get(Uri.parse(url), headers:{"Accept":"application/json"});
  var convertDataToJson = jsonDecode(response.body);
  return convertDataToJson;
}
